require 'Presentation'

describe Presentation do
  it "When testing the code from the terms of reference" do

    book1  = Presentation::Book.new
    book2  = Presentation::Book.new

    author = Presentation::Author.new("Stephen King")

    book1 = author.sign_book(book1) do
      "Best regards!"
    end

    book2 = author.sign_book(book2) do
      "For Sandy with love!"
    end

    expect(book1.sign).to eq "Best regards! Stephen King"
    expect(book2.sign).to eq "For Sandy with love! Stephen King"
  end

  context "When testing the Presentation::Author class" do

    it "check initialize() method" do
      author = Presentation::Author.new("Stephen King")
      expect(author.name).to eq "Stephen King"
    end

    it "check name availability" do
      author = Presentation::Author.new("Stephen King")
      expect {author.name = "t"}.to raise_error(NoMethodError)
      expect(author.name).to eq "Stephen King"
    end

    it "check sign_book() method" do
      author = Presentation::Author.new("Stephen King")
      book_n = Presentation::Book.new
      book_m1 = Presentation::Book.new
      book_0 = Presentation::Book.new
      book_1 = Presentation::Book.new
      book_2 = Presentation::Book.new
      book_3 = Presentation::Book.new

      book_n = author.sign_book(book_n) {"Best, regards!"}
      book_0 = author.sign_book(book_0, 0) {"Best, regards!"}
      book_1 = author.sign_book(book_1, 1) {"Best, regards!"}
      book_2 = author.sign_book(book_2, 2) {"Best, regards!"}

      expect(book_0.sign).to eq "Stephen King Best, regards!"
      expect(book_1.sign).to eq "Best, Stephen King regards!"
      expect(book_2.sign).to eq "Best, regards! Stephen King"
      expect(book_n.sign).to eq "Best, regards! Stephen King"

      expect {author.sign_book(book_m1, -1) {"Best, regards!"}}.to raise_error(RuntimeError, "Вставка имени выходит за границы переданной строки")
      expect {author.sign_book(book_3, 3) {"Best, regards!"}}.to raise_error(RuntimeError, "Вставка имени выходит за границы переданной строки")
    end
  end

  context "When testing the Presentation::Book class" do

    it "check sign availability" do
      book = Presentation::Book.new

      expect(book.sign).to be_nil
      expect {book.sign = "t"}.to raise_error(NoMethodError)
    end

    it "check set_sign method" do
      book = Presentation::Book.new
      book.set_sign("Best, regards!")

      expect(book.sign).to eq "Best, regards!"
      expect {book.set_sign("Other sign")}.to raise_error(RuntimeError, "Книга уже подписана")
    end
  end
end
