# Задание Presentation

## ТЗ

Стивен Кинг на презентации своей новой книги подписывает их поклонникам. Для каждой книги он спрашивает
как ее подписать (строка в блоке), далее он пишет это и в конце ставит свое имя
(которое было указано при инициализаци автора) - в результате в книге появляется строка целиком.

    book1  = Presentation::Book.new
    book2  = Presentation::Book.new

    author = Presentation::Author.new("Stephen King")

    book1 = author.sign_book(book1) do
    "Best regards!"
    end

    book1.sign
    => "Best regards! Stephen King"

    book2 = author.sign_book(book2) do
    "For Sandy with love!"
    end

    book2.sign
    => "For Sandy with love! Stephen King"

*сделать так чтобы имя автора можно было бы вставлять в любую часть строки, предлагаемую читателем

## Основной код

Основной код находятся в файле lib/Presentation.rb

## Тестирование

Rspec-тесты находятся в файле spec/Presentation_spec.rb
Для запуска Rspec-тестов использовать команду

    rspec spec spec/
