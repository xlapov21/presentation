module Presentation
  class Book
    attr_reader :sign

    def set_sign(sign)
      if @sign.nil?
        @sign = sign
      else
        raise "Книга уже подписана"
      end
    end
  end

  class Author
    attr_reader :name

    def initialize (name)
      @name = name
    end

    def sign_book(book, name_index = yield.split(" ").length) #name_index указывает, каким словом поставить имя автора
      sign_array = yield.split(" ")
      if name_index >= 0 && name_index <= sign_array.length
        book.set_sign((sign_array.insert(name_index, @name)).join(" "))
      else
        raise "Вставка имени выходит за границы переданной строки"
      end
      book
    end
  end
end
